package classify_test

import (
	"testing"

	"bitbucket.org/3domfbk/gistools/classify"
	"bitbucket.org/3domfbk/gistools/io"
	. "github.com/smartystreets/goconvey/convey"
)

// TestAggregation to aggregate raster classification data to vector data
func TestAggregation(t *testing.T) {
	Convey("Given a geotiff and a shape file", t, func() {
		raster, err := io.ReadRaster("testdata/stem_trento.tif")
		vector, err2 := io.ReadVector("testdata/stem_trento.shp")
		Convey("It should read them correctly", func() {
			So(err, ShouldBeNil)
			So(raster, ShouldNotBeNil)
			So(err2, ShouldBeNil)
			So(vector, ShouldNotBeNil)
		})

		Convey("It should correctly produce an aggregation raster", func() {
			aggregation := classify.Aggregate(vector, raster)
			So(aggregation, ShouldNotBeNil)

			err = io.WriteRaster("testdata/aggregated.tif", aggregation)
			So(err, ShouldBeNil)
		})
	})
}
