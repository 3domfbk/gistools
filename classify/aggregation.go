package classify

import (
	"image"
	"image/color"

	"bitbucket.org/3domfbk/commontools/geom"
	"bitbucket.org/3domfbk/gistools/types"
	"github.com/volkerp/goquadtree/quadtree"
)

// classification stats for a polygon
type polyStats struct {
	pointPerClass map[int]int
	bestClass     int
}

type polyIndex struct {
	index      int
	dataSource *types.VectorData
}

// BoundingBox returns the bounding box for this polygon
func (p polyIndex) BoundingBox() quadtree.BoundingBox {
	poly := p.dataSource.Polygons[p.index]
	return quadtree.NewBoundingBox(poly.Bounds.Min.X, poly.Bounds.Max.X, poly.Bounds.Min.Y, poly.Bounds.Max.Y)
}

// Aggregate raster classification so that it is uniform with respect to vector data
func Aggregate(vector *types.VectorData, raster *types.Raster) *types.Raster {
	// Create map for polygon statistics
	stats := make(map[int]polyStats)

	// Build quadtree to index vector data
	bb := quadtree.NewBoundingBox(
		raster.X, raster.X+float64(raster.Image.Image.Bounds().Max.X),
		raster.Y, raster.Y-float64(raster.Image.Image.Bounds().Max.Y))
	qt := quadtree.NewQuadTree(bb)
	for i := range vector.Polygons {
		qt.Add(polyIndex{i, vector})
		stats[i] = polyStats{make(map[int]int), -1}
	}

	// Scan all raster pixels
	im := raster.Image.Image

	for y := 0; y < im.Bounds().Dy(); y++ {
		for x := 0; x < im.Bounds().Dx(); x++ {
			cls, _, _, _ := im.At(x, y).RGBA()
			cls = uint32(0xFF * float64(cls) / float64(0xFFFF))
			gx := raster.X + float64(x)*raster.PxSclX
			gy := raster.Y - float64(y)*raster.PxSclY
			intersectingPolys := qt.Query(quadtree.NewBoundingBox(gx-raster.PxSclX, gx+raster.PxSclX, gy-raster.PxSclY, gy+raster.PxSclY))

			for _, p := range intersectingPolys {
				pindex := p.(polyIndex)
				poly := pindex.dataSource.Polygons[pindex.index]
				if poly.Contains(geom.Vec2{X: gx, Y: gy}) {
					sts := stats[pindex.index]
					sts.pointPerClass[int(cls)]++
					stats[pindex.index] = sts
				}
			}
		}
	}

	// Compute best classify_test
	for pid, stat := range stats {
		max := -1
		for cls, npoints := range stat.pointPerClass {
			if npoints > max {
				stat.bestClass = cls
				max = npoints
			}
		}

		stats[pid] = stat
	}

	// Write output raster
	out := types.NewRaster(im.Bounds().Dx(), im.Bounds().Dy(), raster.X, raster.Y, raster.PxSclX, raster.PxSclY)
	for y := 0; y < im.Bounds().Dy(); y++ {
		for x := 0; x < im.Bounds().Dx(); x++ {
			gx := raster.X + float64(x)*raster.PxSclX
			gy := raster.Y - float64(y)*raster.PxSclY
			intersectingPolys := qt.Query(quadtree.NewBoundingBox(gx-raster.PxSclX, gx+raster.PxSclX, gy-raster.PxSclY, gy+raster.PxSclY))

			for _, p := range intersectingPolys {
				pindex := p.(polyIndex)
				poly := pindex.dataSource.Polygons[pindex.index]
				if poly.Contains(geom.Vec2{X: gx, Y: gy}) {
					sts := stats[pindex.index]
					out.Image.Image.(*image.RGBA).Set(x, y, color.RGBA{uint8(sts.bestClass), uint8(sts.bestClass), uint8(sts.bestClass), 255})
				}
			}
		}
	}
	return out
}
