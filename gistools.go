package main

import (
	"log"
	"os"

	"bitbucket.org/3domfbk/gistools/classify"
	"bitbucket.org/3domfbk/gistools/io"

	"github.com/urfave/cli"
)

func main() {
	app := cli.NewApp()
	app.Name = "boom"
	app.Usage = "make an explosive entrance"
	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:  "lang",
			Value: "english",
			Usage: "language for the greeting",
		},
	}
	app.Commands = []cli.Command{
		{
			Name:    "aggregate",
			Aliases: []string{"aggr"},
			Usage:   "aggregate classification raster with vector data",
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:  "lang",
					Value: "english",
					Usage: "language for the greeting",
				},
			},
			Action: func(c *cli.Context) error {
				aggregate(c.Args().Get(0), c.Args().Get(1), "out.tif")
				return nil
			},
		},
	}

	app.Run(os.Args)
}

// aggregates a classification raster by combining it with vector data
func aggregate(rasterPath, vectorPath, outPath string) {
	raster, err := io.ReadRaster(rasterPath)
	if err != nil {
		log.Fatal("Could not open raster, ", err)
	}

	vector, err := io.ReadVector(vectorPath)
	if err != nil {
		log.Fatal("Could not open vector, ", err)
	}

	res := classify.Aggregate(vector, raster)

	// Write output
	err = io.WriteRaster(outPath, res)
	if err != nil {
		log.Fatal("Could not write output raster, ", err)
	}
}
