package io

import (
	"fmt"
	"log"
	"path/filepath"

	"bitbucket.org/3domfbk/commontools/geom"
	"bitbucket.org/3domfbk/gistools/types"
	shp "github.com/jonas-p/go-shp"
)

// ReadVector reads vector data from the specified file
func ReadVector(filename string) (*types.VectorData, error) {
	extension := filepath.Ext(filename)
	switch extension {
	case ".shp", ".SHP", ".Shp":
		return readShp(filename)
	}

	return nil, fmt.Errorf("Format not supported: %s", extension)
}

// Reads a shapefile
func readShp(filename string) (*types.VectorData, error) {
	// open a shapefile for reading
	shape, err := shp.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer shape.Close()

	result := types.NewVectorData(shape.AttributeCount())

	// loop through all features in the shapefile
	for shape.Next() {
		_, p := shape.Shape()
		pol := p.(*shp.Polygon)

		tPol := geom.NewPolygon2(len(pol.Points))

		for _, p := range pol.Points {
			tPol.AddPoint(geom.Vec2{X: p.X, Y: p.Y})
		}

		result.AddPolygon(tPol)
	}
	return result, err
}
