package io_test

import (
	"testing"

	"bitbucket.org/3domfbk/gistools/io"
	. "github.com/smartystreets/goconvey/convey"
)

func TestRaster(t *testing.T) {
	Convey("Given a tiff file", t, func() {
		raster, err := io.ReadRaster("testdata/qgis_small.tif")

		So(err, ShouldBeNil)
		So(raster.X, ShouldAlmostEqual, 1.236325936512217e+06)
		So(raster.Y, ShouldAlmostEqual, 5.79403423278085e+06)
		So(raster.PxSclX, ShouldAlmostEqual, 0.5971642833948135)
		So(raster.PxSclY, ShouldAlmostEqual, 0.5971642833948135)
	})
}

func TestVector(t *testing.T) {
	Convey("Given a .shp file", t, func() {
		vector, err := io.ReadVector("testdata/qgis_small.shp")
		So(err, ShouldBeNil)
		So(vector.Polygons, ShouldHaveLength, 1)
		So(vector.Polygons[0].Points, ShouldHaveLength, 5)
	})
}
