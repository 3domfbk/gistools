package io

import (
	"fmt"
	"path/filepath"

	"bitbucket.org/3domfbk/gistools/types"
	"bitbucket.org/3domfbk/imagetools/io"
	imt "bitbucket.org/3domfbk/imagetools/types"
)

// ReadRaster reads a raster from a file
func ReadRaster(filename string) (*types.Raster, error) {
	extension := filepath.Ext(filename)
	switch extension {
	case ".tif", ".tiff", ".TIFF", ".Tiff":
		image, err := io.ReadMetaImage(filename)
		if err != nil {
			return nil, err
		}

		mtp, ok := image.Float64Tags(imt.TagModelTiePoint)
		if !ok {
			return nil, fmt.Errorf("Raster image should have ModelTiePointTag")
		}

		mps, ok := image.Float64Tags(imt.TagModelPixelScale)
		if !ok {
			return nil, fmt.Errorf("Raster image should have ModelPixelScale")
		}

		x := mtp[3]
		y := mtp[4]
		pxSclX := mps[0]
		pxSclY := mps[1]
		raster := types.Raster{Image: image, X: x, Y: y, PxSclX: pxSclX, PxSclY: pxSclY}
		return &raster, err
	}

	return nil, fmt.Errorf("Format not supported: %s", extension)
}

// WriteRaster writes the specified raster to file
func WriteRaster(filename string, raster *types.Raster) error {
	raster.Image.SetFloat64s(imt.TagModelTiePoint, []float64{0, 0, 0, raster.X, raster.Y, 0})
	raster.Image.SetFloat64s(imt.TagModelPixelScale, []float64{raster.PxSclX, raster.PxSclY, 0})
	return io.WriteMetaImage(filename, raster.Image)
}
