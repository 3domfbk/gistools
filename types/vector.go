package types

import "bitbucket.org/3domfbk/commontools/geom"

// VectorData is a collection of polygons
type VectorData struct {
	Polygons []geom.Polygon2
}

// NewVectorData creates a new VectorData object
func NewVectorData(initialSize int) *VectorData {
	return &VectorData{make([]geom.Polygon2, 0, initialSize)}
}

// AddPolygon adds a polygon to the data
func (v *VectorData) AddPolygon(p *geom.Polygon2) {
	v.Polygons = append(v.Polygons, *p)
}
