package types

import (
	"image"

	"bitbucket.org/3domfbk/imagetools/types"
)

// Raster is a geo-referenced image
type Raster struct {
	Image          *types.MetaImage
	X, Y           float64
	PxSclX, PxSclY float64
}

// NewRaster creates an empty raster of the specified size
func NewRaster(w, h int, X, Y, PxSclX, PxSclY float64) *Raster {
	r := Raster{
		Image: types.NewMetaImage(),
		X:     X, Y: Y,
		PxSclX: PxSclX, PxSclY: PxSclY,
	}

	r.Image.SetUInt16(types.TagImageWidth, uint16(w))
	r.Image.SetUInt16(types.TagImageLength, uint16(h))
	r.Image.SetUInt16s(types.TagBitsPerSample, []uint16{8, 8, 8})
	r.Image.SetUInt16s(types.TagSampleFormat, []uint16{1, 1, 1})
	r.Image.SetUInt16(types.TagCompression, types.TagCompressionNone)
	r.Image.SetUInt16(types.TagPhotometricInterpolation, types.TagPIRGB)
	r.Image.SetUInt16(types.TagSamplesPerPixel, uint16(3))
	r.Image.SetUInt16(types.TagPlanarConfiguration, uint16(1))

	r.Image.SetFloat64s(types.TagModelPixelScale, []float64{PxSclX, PxSclY, 0})
	r.Image.SetFloat64s(types.TagModelTiePoint, []float64{0, 0, 0, X, Y, 0})

	r.Image.Image = image.NewRGBA(image.Rect(0, 0, w, h))
	return &r
}
